<?
CModule::IncludeModule('iblock');

AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", Array("MyClass", "OnAfterIBlockSectionUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockSectionDelete", Array("MyClass", "OnAfterIBlockSectionUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("MyClass", "OnAfterIBlockSectionUpdateHandler"));

class MyClass
{
    function OnAfterIBlockSectionUpdateHandler(&$arFields)
    {
        
    	switch ($arFields["IBLOCK_ID"]) {		
		case 33:			
			$file_name1 = $_SERVER["DOCUMENT_ROOT"]."/services.php";
			$file_name2 = $_SERVER["DOCUMENT_ROOT"]."/services_depth.php";
			break;
		case 45:			
			$file_name1 = $_SERVER["DOCUMENT_ROOT"]."/product.php";
			$file_name2 = $_SERVER["DOCUMENT_ROOT"]."/product_depth.php";
			break;
		default:
			return false;
		}
       
	    $ar_SectionList = array();
		$ar_DepthLavel = array();

		$rs_Section = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $arFields["IBLOCK_ID"], 'ACTIVE' => 'Y'),false, array("NAME","DEPTH_LEVEL","ID","IBLOCK_SECTION_ID","LEFT_MARGIN","SECTION_PAGE_URL","SUB_SECTION") );
		while($ar_Section = $rs_Section->GetNext(true, false))
		{
			$ar_SectionList[$ar_Section['ID']] = $ar_Section;
			$ar_DepthLavel[] = $ar_Section['DEPTH_LEVEL'];
		}

		$json1 = json_encode($ar_SectionList, JSON_UNESCAPED_UNICODE);
		$json2 = json_encode($ar_DepthLavel);
		file_put_contents($file_name1, $json1);
		file_put_contents($file_name2, $json2);
    }
}

?>