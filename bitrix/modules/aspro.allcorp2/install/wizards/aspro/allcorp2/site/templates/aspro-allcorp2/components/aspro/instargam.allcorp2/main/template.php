<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);
if(isset($_POST["AJAX_REQUEST_INSTAGRAM"]) && $_POST["AJAX_REQUEST_INSTAGRAM"] == "Y"):
	$inst=new CInstargramAllcorp2($arParams["TOKEN"]);
	$arInstagramPosts=$inst->getInstagramPosts();
	if($arInstagramPosts["error"]["message"]):
		global $USER;
		if(!is_object($USER)){
		     $USER = new CUser();
		}
		if($USER->IsAdmin()):?>
			<div class="alert alert-danger">
				<strong>Error: </strong><?=$arInstagramPosts["error"]["message"]?>
			</div>
		<?endif;
	endif;

	$arInstagramUser['name'] = $arData['data'][0]['username'];
	//$arInstagramUser=$inst->getInstagramUser();
	if($arInstagramPosts && !$arInstagramPosts["error"]["message"]):?>
		<div class="item-views front blocks">
			<h3 class="text-center"><?=($arParams["TITLE"] ? $arParams["TITLE"] : GetMessage("TITLE"));?></h3>
			<div class="instagram clearfix">
				<div class="container">
					<?$index = 0;?>
					<div class="items row flexbox">
						<div class="item user">
							<div class="body2">
								<div class="title"><h4><?=GetMessage('INSTAGRAM_TITLE');?></h4></div>
								<div class="description"><?=GetMessage('INSTAGRAM_DESCRIPTION');?></div>
								<div class="link"><a href="https://www.instagram.com/<?=$arInstagramUser['name']?>/" target="_blank"><?=$arInstagramUser['name']?></a></div>
							</div>
						</div>
						<?foreach ($arInstagramPosts['data'] as $arItem):?>
							<?$arItem['LINK'] = $arItem['thumbnail_url'] ? $arItem['thumbnail_url'] : $arItem['media_url'];?>
							<div class="item">
								<div class="image" style="background:url(<?=$arItem['LINK'];?>) center center/cover no-repeat;"></div>
								<div class="desc">
									<div class="wrap">
										<div class="date font_upper"><?=CAllcorp2::showIconSvg("date_insta", SITE_TEMPLATE_PATH.'/images/svg/instagram_mainpage.svg');?><span><?=FormatDate('d F', $arItem['timestamp'], 'SHORT');?></span></div>
										<?if($arItem['caption']):?>
											<div class="text font_xs"><?=$arItem['caption'];?></div>
										<?endif;?>
										<a href="<?=$arItem['permalink']?>" target="_blank" rel="nofollow"></a>
									</div>
								</div>
							</div>
							<?if ($index == 3) break;?>
							<?++$index;?>
						<?endforeach;?>
					</div>
				</div>
			</div>
		</div>
	<?endif;?>
<?else:?>
	<div class="row margin0">
		<div class="maxwidth-theme">
			<div class="col-md-12">
				<div class="instagram_ajax loader_circle"></div>
			</div>
		</div>
	</div>
<?endif;?>