<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?//$this->setFrameMode(true);?>
	<?if($arResult['SECTIONS']):?>
			<div class="text_before_items">
				<?echo $arResult['DESCRIPTION'];?>
			</div>
				<?foreach($arResult['SECTIONS'] as $si => $arSection):?>
				<?
				$arSectionButtons = CIBlock::GetPanelButtons($arSection['IBLOCK_ID'], 0, $arSection['ID'], array('SESSID' => false, 'CATALOG' => true));
				$this->AddEditAction($arSection['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arSection['IBLOCK_ID'], 'SECTION_EDIT'));
				$this->AddDeleteAction($arSection['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arSection['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
					<div class="item-views catalog sections">
						<div class="items row margin0 row_block flexbox nmac">
						<?foreach($arSection['ITEMS'] as $i => $arItem):?>
							<?
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);
							$bImage = (isset($arItem['FIELDS']['PREVIEW_PICTURE']) && $arItem['PREVIEW_PICTURE']['SRC']);
							$imageSrc = ($bImage ? $arItem['PREVIEW_PICTURE']['SRC'] : false);
							$imageDetailSrc = ($bImage ? $arItem['DETAIL_PICTURE']['SRC'] : false);
							?>
							<div class="item  childs slice-item left">
								<?if($bImage):?>
								<div class="image image-edit" style="height:240px !important;">
									<?/*if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="blink">
											<?elseif($arItem['FIELDS']['DETAIL_PICTURE']):?>
										<a href="<?=$imageDetailSrc?>" alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" class="img-inside fancybox">
										<?endif;*/?>
									<a href="<?=$arItem['PROPERTIES']['SITE']['VALUE']?>" target="_blank"><img class="img-responsive" src="<?=$imageSrc?>" alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" class="img-responsive" /></a>
										<?/*if($bDetailLink):?></a>
										<?elseif($arItem['FIELDS']['DETAIL_PICTURE']):?><span class="zoom"><i class="fa fa-16 fa-white-shadowed fa-search"></i></span></a>
										<?endif;*/?>
									</div>
								<?endif;?>
								<?if(strlen($arItem['FIELDS']['NAME'])):?>
									<div class="info info-edit" style="padding-left: 0px;">
										<div class="title title_edit">
											<?//if($bDetailLink):?><a href="<?=$arItem['PROPERTIES']['SITE']['VALUE']?>" target="_blank" class="dark-color text-edit" style="font-size: 14px !important;"><?//endif;?>
											<?=$arItem['NAME']?>
											<?//if($bDetailLink):?></a><?//endif;?>
										</div>
										<?//echo "<pre>".print_r($arItem,true)."</pre>";?>
									</div>
								<?endif;?>
						</div>
						<?endforeach;?>
					</div>
				</div>
	<?endforeach;?>
	<?endif;?>