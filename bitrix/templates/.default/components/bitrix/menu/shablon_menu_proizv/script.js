var jsvhover = function()
{
	var menuDiv = document.getElementById("vertical-multilevel-menu");
	if (!menuDiv)
		return;

  var nodes = menuDiv.getElementsByTagName("li");
  for (var i=0; i<nodes.length; i++) 
  {
    nodes[i].onmouseover = function()
    {
      this.className += " jsvhover";
    }

    nodes[i].onmouseout = function()
    {
      this.className = this.className.replace(new RegExp(" jsvhover\\b"), "");
    }
  }
}

if (window.attachEvent) 
	window.attachEvent("onload", jsvhover); 


$(document).ready(function() {
	$(".section_down").css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	$(".section_down").parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	$(".section_down").prev().prev().children().removeClass("fa-angle-down");
	$(".section_down").prev().prev().children().addClass("fa-angle-up");

	/*$(".section_down").parent().parent().parent(".acc-body").css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	$(".section_down").parent().parent().parent(".acc-body").parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	$(".section_down").parent().parent().parent(".acc-body").prev().prev().children().removeClass("fa-angle-down");
	$(".section_down").parent().parent().parent(".acc-body").prev().prev().children().addClass("fa-angle-up");*/

	$(".item_down").next().children().css({"font-weight":"900","font-size":"16px","border-bottom":"2px LightSkyBlue solid"});

	var parent = $(".section_down,.item_down").parent().parent().parent(".acc-body");
	parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	parent.prev().prev().children().removeClass("fa-angle-down");
	parent.prev().prev().children().addClass("fa-angle-up");

	var parent_parent = $(".section_down,.item_down").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body");
	parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	parent_parent.prev().prev().children().removeClass("fa-angle-down");
	parent_parent.prev().prev().children().addClass("fa-angle-up");

	var parent_parent_parent = $(".section_down,.item_down").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body");
	parent_parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	parent_parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	parent_parent_parent.prev().prev().children().removeClass("fa-angle-down");
	parent_parent_parent.prev().prev().children().addClass("fa-angle-up");

	var parent_parent_parent_parent = $(".section_down,.item_down").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body").parent().parent().parent(".acc-body");
	parent_parent_parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	parent_parent_parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	parent_parent_parent_parent.prev().prev().children().removeClass("fa-angle-down");
	parent_parent_parent_parent.prev().prev().children().addClass("fa-angle-up");

	$('#accord .acc-head').on('click', f_ac);


	//var col = $(this).css('background');


	$('.a_edit').hover( function(){
					//var col = $(this).children().css('background-color');
					//alert(col);
					//$(this).removeAttr('style');
					//$(this).toggleClass('is-hov');
					$(this).toggleClass('is-hov');
					$(this).parent().prev().children().toggleClass('is-hov1');
					//$(this).children().toggleClass('is-hov');
                });
	/*$('.main_ul').on("mouseout", 
                function(){
					alert(col);
                    $(this).css('background','red');
					$(this).children().css('background','navy');
                });*/
});
 
function f_ac(){
  	$('#accord > .acc-body').not($(this).next().next()).slideUp(300);
	$(this).next().next().css("margin-top","12px");
	$(this).parent().css("padding-bottom","0px");
	$(this).parent().css("border-bottom","none");
	$(this).next().next().css("border-top","1px solid white");
	$(this).next().next().slideToggle(700, function(){
		if ($(this).prev().prev().children().hasClass("fa-angle-down")){
			$(this).prev().prev().children().removeClass("fa-angle-down");
			$(this).prev().prev().children().addClass("fa-angle-up"); 
		}else{
			$(this).prev().prev().parent().parent().css("background","#1a5687");
			$(this).prev().prev().children().removeClass("fa-angle-up");
			$(this).prev().prev().children().addClass("fa-angle-down");
			$(this).parent().css("padding-bottom","12px");
			$(this).parent().css("border-bottom","1px solid white");
		}
	});
}