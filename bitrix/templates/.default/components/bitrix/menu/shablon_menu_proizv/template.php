<?php
if (CModule::IncludeModule('iblock')){ 
   $codes = ['catalogg', 'proizvodstvo', 'aspro_allcorp2_services']; 
	$page = $APPLICATION->GetCurPage();
	$p = explode("/", $page);
	$pp = $p[count($p)-2];
	switch ($p[1]) {
    case 'proizvodstvo':
        $code = $codes[1];
        break;
    case 'services':
        $code = $codes[2];
        break;
    case 'product':
        $code = $codes[0];
        break;
	} 
    $resc = CIBlock::GetList(Array(), Array('CODE' => $code), false);
    while($arrc = $resc->Fetch())
      $c_id=$arrc["ID"];

	$rsSect = CIBlockSection::GetList(array(),array('IBLOCK_ID' => $c_id, '=CODE' => $pp, 'ACTIVE' => 'Y'));
	if ($arSecti = $rsSect->Fetch())
	{
		$idsect = $arSecti['ID'];
		$namesect = $arSecti['NAME'];
	}

	if(count($p)>3 && !$idsect){
		$pp = $p[count($p)-3];
		$rsSect = CIBlockSection::GetList(array(),array('IBLOCK_ID' => $c_id, '=CODE' => $pp, 'ACTIVE' => 'Y'));
		if ($arSecti = $rsSect->Fetch())
		{
			$idsect = $arSecti['ID'];
			$namesect = $arSecti['NAME'];
		}
	}
}
?>

<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$rs_Section = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $c_id, 'ACTIVE' => 'Y'));
$ar_SectionList = array();
$ar_DepthLavel = array();
while($ar_Section = $rs_Section->GetNext(true, false))
{
	$ar_SectionList[$ar_Section['ID']] = $ar_Section;
	$ar_DepthLavel[] = $ar_Section['DEPTH_LEVEL'];
}

$ar_DepthLavelResult = array_unique($ar_DepthLavel);
rsort($ar_DepthLavelResult);

$i_MaxDepthLevel = $ar_DepthLavelResult[0];

for( $i = $i_MaxDepthLevel; $i > 1; $i-- )
{
	foreach ( $ar_SectionList as $i_SectionID => $ar_Value )
	{
		if( $ar_Value['DEPTH_LEVEL'] == $i )
		{
			$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][] = $ar_Value;
			unset( $ar_SectionList[$i_SectionID] );
		}
	}
}

function __sectSort($a, $b)
{
	if ($a['SORT'] == $b['SORT']) {
		return 0;
	}
	return ($a['SORT'] < $b['SORT']) ? -1 : 1;
}

usort($ar_SectionList, "__sectSort");
?>

<div id="accord">
	<div  class="main_sect">
		<a href = "/proizvodstvo/">
			<i class="fa fa-wrench" aria-hidden="true"></i>
			Производство
		</a>
	</div>
	<div class="main_sect">
		<a href = "/services/">
			<i class="fa fa-handshake-o" aria-hidden="true"></i>
			Услуги
		</a>
	</div>
	<div class="main_sect">
		<a href = "/product/">
		<i class="fa fa-suitcase" aria-hidden="true"></i>
			Каталог
		</a>
</div>

<?
function __recursRenderMenu($ar_Items,$idsect)
{	
	$i=1;
    foreach ($ar_Items as $ar_Value)
	{
		$j=$ar_Value["DEPTH_LEVEL"];
		$style = 'style="background:';
		if($j==2)
			$color = '#0088cc;"';
		elseif($j==3)
			$color = '#04477e;"';
		elseif($j==4)
			$color = '#739fc3;"';
		elseif($j==5)
			$color = '#a4b3c0;"';
		else
			$color = '#1a5687;"';
        if(count($ar_Value['SUB_SECTION']) > 0)
		{?>
			<div class="main_ul" rel="<?=$j?>">
				<div class="ac-head" <?=$style.$color;?>>
					<div class="acc-head">
						<i  class="fa fa-angle-down" aria-hidden="true">
						</i>
					</div>
					<div class="a_before">
						<a class="a_edit" href="<?=$ar_Value['SECTION_PAGE_URL']?>"> 
							<?=$ar_Value['NAME']?>
						</a>
					</div>
					<div class="acc-body <?if ($ar_Value['ID'] == $idsect) echo 'section_down';?>" rel="<? echo $ar_Value['ID'].'  '.$idsect?>">
						<? echo __recursRenderMenu($ar_Value['SUB_SECTION'],$idsect);?>
					</div>
                </div>
            </div> 
			<? $i++;               
        }    
        else
        {?>
			<div class="main_ul" rel="<?=$j?>">
				<div class="ac-head" <?=$style.$color;?>>
					<div class="acc-head <?if ($ar_Value['ID'] == $idsect) echo 'item_down';?>" rel="<? echo $ar_Value['ID'].'  '.$idsect?>">
					</div>
					<div class="a_before">
						<a class="a_edit" href="<?=$ar_Value['SECTION_PAGE_URL']?>">
							<?=$ar_Value['NAME']?>
						</a>
					</div>
				</div>
			</div> 
        <?}    
    }  
	$j++;  
}

echo  __recursRenderMenu($ar_SectionList,$idsect);

?>
</div>