var jsvhover = function()
{
	var menuDiv = document.getElementById("vertical-multilevel-menu");
	if (!menuDiv)
		return;

  var nodes = menuDiv.getElementsByTagName("li");
  for (var i=0; i<nodes.length; i++) 
  {
    nodes[i].onmouseover = function()
    {
      this.className += " jsvhover";
    }

    nodes[i].onmouseout = function()
    {
      this.className = this.className.replace(new RegExp(" jsvhover\\b"), "");
    }
  }
}

if (window.attachEvent) 
	window.attachEvent("onload", jsvhover); 

$(window).load(function(){
	var e = $(".section_down");
	var ell = e.prev().prev().children();
	e.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	e.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	ell.removeClass("fa-angle-down");
	ell.addClass("fa-angle-up");

	$(".item_down").next().children().css({"font-weight":"900","font-size":"16px","border-bottom":"2px LightSkyBlue solid"});

	var parent = $(".section_down,.item_down").parent().parent().parent(".acc-body");
	parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
	parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
	var el1 = parent.prev().prev().children();
	el1.removeClass("fa-angle-down");
	el1.children().addClass("fa-angle-up");

	if (typeof parent !== 'undefined' && parent.parent().parent().parent(".acc-body").length){
		var parent_parent = parent.parent().parent().parent(".acc-body");
		parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
		parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
		var el2 = parent_parent.prev().prev().children();
		el2.removeClass("fa-angle-down");
		el2.addClass("fa-angle-up");
	}

	if (typeof parent_parent !== 'undefined' && parent_parent.parent().parent().parent(".acc-body").length){
		var parent_parent_parent = parent_parent.parent().parent().parent(".acc-body");
		parent_parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
		parent_parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
		var el3 = parent_parent_parent.prev().prev().children();
		el3.removeClass("fa-angle-down");
		el3.addClass("fa-angle-up");
	}

	if (typeof parent_parent_parent !== 'undefined' && parent_parent_parent.parent().parent().parent(".acc-body").length){
		var parent_parent_parent_parent = parent_parent_parent.parent().parent().parent(".acc-body");
		parent_parent_parent_parent.css({ "display": "block", "margin-top": "12px", "border-top": "1px solid white" });
		parent_parent_parent_parent.parent().css({ "padding-bottom": "0px", "border-bottom": "none"});
		var el4 = parent_parent_parent_parent.prev().prev().children();
		el4.removeClass("fa-angle-down");
		el4.addClass("fa-angle-up");
	}

	$('#accordeon').find('.acc-head').on('click', f_acc);

	$('a.a_edit').hover(function(){
					$(this).toggleClass('is-hov');
					$(this).parent().prev().children().toggleClass('is-hov1');
                });
});

function f_acc(){
	var thisnext = $(this).next().next();
  	$('#accordeon > acc-body').not(thisnext).slideUp(300);

	thisnext.css({"margin-top":"12px","border-top":"1px solid white"}); //для .acc-body
	$(this).parent('.ac-head').css({"padding-bottom":"0px","border-bottom":"none"});  //для .ac-head

	thisnext.slideToggle(700, function(){
		var elp = $(this).prev().prev();
		var el = elp.children();
		if (el.hasClass("fa-angle-down")){
			el.removeClass("fa-angle-down").addClass("fa-angle-up"); //для .fa.fa-angle-down /меняем down на up
		}else{
			elp.parent().parent().css("background","#1a5687"); //для main_ul
			el.removeClass("fa-angle-up").addClass("fa-angle-down"); //для .fa.fa-angle-up
			$(this).parent().css({"padding-bottom":"12px","border-bottom":"1px solid white"}); //для .ac-head
		}
	});
}