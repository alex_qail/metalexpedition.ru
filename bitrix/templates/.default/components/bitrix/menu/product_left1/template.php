<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>
<?$this->setFrameMode(true);?>

<?
if (CModule::IncludeModule('iblock')) {
    $obSections = CIBlockSection::GetList(
        array("SORT" => "ASC"),
        array("IBLOCK_ID" => 45, "ACTIVE" => "Y", "DEPTH_LEVEL" => 1),
        false,
        array("ID", "NAME", "SECTION_PAGE_URL")
    );
    
    while ($resSections = $obSections -> GetNext()) {
        $arSections[] = $resSections;
		//пример выборки дерева подразделов для раздела 
		/*$rsParentSection = CIBlockSection::GetByID($resSections['ID']);
		if ($arParentSection = $rsParentSection->GetNext())
		{
		   $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'DEPTH_LEVEL' => 2);
		   $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
		   while ($arSect = $rsSect->GetNext())
		   {
			   $arSections[]["CHILD"] = $arSect;
		   }
		}*/
    }
}
/*foreach ($arSections as $child){
	echo "<pre>".print_r($child['CHILD']['NAME'],true)."</pre>";
}*/
?>

<aside class="sidebar">
    <ul class="nav nav-list side-menu">
		<li style="background-color:white;color:#1a5687;border: 1px solid #1a5687;">
			<a href = "/proizvodstvo/" style="color:#1a5687;font-size: 16px;font-weight: bold;border-bottom: 1px solid #1a5687;">
		<i class="fa fa-wrench" aria-hidden="true" style="margin-right: 10px;"></i>
		Производство
	</a>
	<a href = "/services/" style="color:#1a5687;font-size: 16px;font-weight: bold;border-bottom: 1px solid #1a5687;">
		<i class="fa fa-handshake-o" aria-hidden="true" style="margin-right: 10px;"></i>
		Услуги
	</a>
	<a href = "/product/" style="color:#1a5687;font-size: 16px;font-weight: bold;">
		<i class="fa fa-suitcase" aria-hidden="true" style="margin-right: 10px;"></i>
		Каталог
	</a>

</li>
        <?
        foreach($arSections as $arItem):
            $arSearch = [
                "http",
                "https",
                ":",
                "//"
            ];
            $trimUrl = str_replace($arSearch, "", $APPLICATION -> GetCurDir());
            $sectionUrl = stristr($trimUrl, $arItem["SECTION_PAGE_URL"]);
        ?>
            <li class="<?=($sectionUrl ? "active" : "")?>">
                <a href="<?=$arItem["SECTION_PAGE_URL"]?>">
                    <?=$arItem["NAME"]?>
                </a>
				<?//ShowSubItems($arItem);?>
            </li>

        <?endforeach;?>
    </ul>
</aside>

<?
/*if(!function_exists("ShowSubItems")){
	function ShowSubItems($arItem){
		?>
		<?if($arItem["SELECTED"] && $arItem["CHILD"]):?>
			<?$noMoreSubMenuOnThisDepth = false;?>
			<div class="submenu-wrapper">
				<ul class="submenu">
					<?foreach($arItem["CHILD"] as $arSubItem):?>
					<li class="<?=($arSubItem["SELECTED"] ? "active" : "")?><?=($arSubItem["CHILD"] ? " child" : "")?>">
							<a href="<?=$arSubItem["LINK"]?>"><?=$arSubItem["TEXT"]?></a>
							<?if(!$noMoreSubMenuOnThisDepth):?>
								<?ShowSubItems($arSubItem);?>
							<?endif;?>
						</li>
						<?$noMoreSubMenuOnThisDepth |= CAllcorp2::isChildsSelected($arSubItem["CHILD"]);?>
					<?endforeach;?>
				</ul>
			</div>
		<?endif;?>
		<?
	}
}*/
?>

<?/*//if($arResult):?>
	<aside class="sidebar">
		<ul class="nav nav-list side-menu">
			<?foreach($arSections as $arItem):?>
				<?
					$arSearch = [
						"http",
						"https",
						":",
						"//"
					];
					$trimUrl = str_replace($arSearch, "", $APPLICATION -> GetCurDir());
					$sectionUrl = stristr($trimUrl, $arItem["SECTION_PAGE_URL"]);
				?>
				<li class="<?=($arItem["SELECTED"] ? "active" : "")?> <?=($arItem["CHILD"] && !isset($arItem["NO_PARENT"]) ? "child" : "")?>">
					<a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=(isset($arItem["PARAMS"]["BLOCK"]) && $arItem["PARAMS"]["BLOCK"] ? $arItem["PARAMS"]["BLOCK"] : "");?><?=$arItem["NAME"]?></a>
					<?ShowSubItems($arItem);?>
				</li>
			<?endforeach;?>
		</ul>
	</aside>
<?//endif;*/?>