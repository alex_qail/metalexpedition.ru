<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?><!DOCTYPE html>
<?if(CModule::IncludeModule("aspro.allcorp2"))
	$arThemeValues = CAllcorp2::GetFrontParametrsValues(SITE_ID);
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>" class="<?=($_SESSION['SESS_INCLUDE_AREAS'] ? 'bx_editmode ' : '')?><?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0' ) ? 'ie ie7' : ''?> <?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0' ) ? 'ie ie8' : ''?> <?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0' ) ? 'ie ie9' : ''?>">
	<head>
		<?global $APPLICATION;?>
		<?IncludeTemplateLangFile(__FILE__);?>
		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowMeta("viewport");?>
		<?$APPLICATION->ShowMeta("HandheldFriendly");?>
		<?$APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes");?>
		<?$APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style");?>
		<?$APPLICATION->ShowMeta("SKYPE_TOOLBAR");?>
		<?$APPLICATION->ShowHead();?>
		<?$APPLICATION->AddHeadString('<script>BX.message('.CUtil::PhpToJSObject($MESS, false).')</script>', true);?>
		<?if(CModule::IncludeModule("aspro.allcorp2")) {CAllcorp2::Start(SITE_ID);}?>
		<script>
			$(window).load(function () {
				/*setTimeout(function () {
					$(".page-loader").addClass("loaded");
}, 1000);*/

});

		</script>
		<style>	
/*
*
* Page Loaders
* --------------------------------------------------
*/
			/*.page-loader {
	position: fixed;
	left: 0;
	top: 0;
	bottom: 0;
	right: 0;
	float: left;
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 20px;
	z-index: 9999999;
	background: #fff;
	transition: .3s all ease;
}

.page-loader.loaded {
	opacity: 0;
	visibility: hidden;
	z-index: -1;
}

.page-loader.ending {
	display: none;
}

.page-loader .page-loader-body {
	text-align: center;
}

[data-x-mode="design-mode"] .page-loader {
	display: none;
}

[data-x-mode="true"] .caption .title {
	pointer-events: auto;
}

.cssload-loader {
	position: relative;
	left: calc(50% - 31px);
	width: 62px;
	height: 62px;
	border-radius: 50%;
	perspective: 780px;
}

.cssload-inner {
	position: absolute;
	width: 100%;
	height: 100%;
	box-sizing: border-box;
	border-radius: 50%;
}

.cssload-inner.cssload-one {
	left: 0%;
	top: 0%;
	animation: cssload-rotate-one 1.15s linear infinite;
	border-bottom: 3px solid black;
}

.cssload-inner.cssload-two {
	right: 0%;
	top: 0%;
	animation: cssload-rotate-two 1.15s linear infinite;
	border-right: 3px solid black;
}

.cssload-inner.cssload-three {
	right: 0%;
	bottom: 0%;
	animation: cssload-rotate-three 1.15s linear infinite;
	border-top: 3px solid black;
}

@keyframes cssload-rotate-one {
	0% {
		transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
	}
	100% {
		transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
	}
}

@keyframes cssload-rotate-two {
	0% {
		transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
	}
	100% {
		transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
	}
}

@keyframes cssload-rotate-three {
	0% {
		transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
	}
	100% {
		transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
	}
		}*/
</style>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVHS3F9');</script>
<!-- End Google Tag Manager -->
	</head>

	<?$bIndexBot = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false);?>
	<body class="<?=($bIndexBot ? "wbot" : "");?> <?=(CModule::IncludeModule("aspro.allcorp2") ? CAllcorp2::getConditionClass() : '');?> mheader-v<?=$arThemeValues["HEADER_MOBILE"];?> footer-v<?=strtolower($arThemeValues['FOOTER_TYPE']);?> fill_bg_<?=strtolower($arThemeValues['SHOW_BG_BLOCK']);?> header-v<?=$arThemeValues["HEADER_TYPE"];?> title-v<?=$arThemeValues["PAGE_TITLE"];?><?=($arThemeValues['ORDER_VIEW'] == 'Y' && $arThemeValues['ORDER_BASKET_VIEW']=='HEADER'? ' with_order' : '')?><?=($arThemeValues['CABINET'] == 'Y' ? ' with_cabinet' : '')?><?=(intval($arThemeValues['HEADER_PHONES']) > 0 ? ' with_phones' : '')?>">
		<!--<div class="page-loader">
			<div>
				<div class="page-loader-body">
					<div class="cssload-loader">
						<div class="cssload-inner cssload-one"></div>
						<div class="cssload-inner cssload-two"></div>
						<div class="cssload-inner cssload-three"></div>
					</div>
				</div>
			</div>
		</div>-->
<?

		if(!isset($_COOKIE['current_region'])){
			$_COOKIE['current_region']=2;
		}
		//echo "<pre>".print_r($_COOKIE['current_region'],true)."</pre>";
//phpinfo();   echo "<pre>".print_r($_COOKIE['catalogViewMode'],true)."</pre>";
?>


		<div id="panel"><?$APPLICATION->ShowPanel();?></div>

		<div id="id_title" style="display:none;">
			<?
				$APPLICATION->ShowTitle(false);

			?>
		</div>

		<?if(!(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false)):?>
			<script type="text/javascript">
				$(document).ready(function(){
					$.ajax({
					url: '<?=SITE_TEMPLATE_PATH?>/asprobanner.php' + location.search,
					type: 'post',
					success: function(html){
						if(!$('.form_demo-switcher').length){
							$('body').append(html);
						}
					}
				});
				/*var t= $("#id_title_my").text();
				var y = t[0].toLowerCase();
				var r = t.slice(1);
				$(".section_title_l").text(y+r);
				$(".section_title").text(t);*/
				$(".add_phone > div > .svg").hide();
				$(".add_address > div > .svg").hide();
			});
			</script>
		<?endif;?>
		<?//include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/asprobanner.php");?>
		<?if(!CModule::IncludeModule("aspro.allcorp2")):?>
			<?$APPLICATION->SetTitle(GetMessage("ERROR_INCLUDE_MODULE_ALLCORP2_TITLE"));?>
			<?$APPLICATION->IncludeFile(SITE_DIR."include/error_include_module.php");?>
			<?die();?>
		<?endif;?>

		<?CAllcorp2::SetJSOptions();?>
		<?global $arTheme;?>
		<?$arTheme = $APPLICATION->IncludeComponent("aspro:theme.allcorp2", "", array(), false);?>
		<?include_once('defines.php');?>

		<?CAllcorp2::get_banners_position('TOP_HEADER');?>
		<div class="visible-lg visible-md title-v<?=$arTheme["PAGE_TITLE"]["VALUE"];?><?=($isIndex ? ' index' : '')?>">
			<?CAllcorp2::ShowPageType('header');?>
		</div>

		<?CAllcorp2::get_banners_position('TOP_UNDERHEADER');?>

		<?if($arTheme["TOP_MENU_FIXED"]["VALUE"] == 'Y'):?>
			<div id="headerfixed">
				<?CAllcorp2::ShowPageType('header_fixed');?>
			</div>
		<?endif;?>

		<div id="mobileheader" class="visible-xs visible-sm">
			<?CAllcorp2::ShowPageType('header_mobile');?>
			<div id="mobilemenu" class="<?=($arTheme["HEADER_MOBILE_MENU_OPEN"]["VALUE"] == '1' ? 'leftside':'dropdown')?>">
				<?CAllcorp2::ShowPageType('header_mobile_menu');?>
			</div>
		</div>

		<div class="body <?=($isIndex ? 'index' : '')?> hover_<?=$arTheme["HOVER_TYPE_IMG"]["VALUE"];?>">
			<div class="body_media"></div>

			<div role="main" class="main banner-auto">
				<?if(!$isIndex && !$is404 && !$isForm):?>
					<?$APPLICATION->ShowViewContent('section_bnr_content');?>
					<?if($APPLICATION->GetProperty("HIDETITLE")!=='Y'):?>
						<!--title_content-->
						<? CAllcorp2::ShowPageType('page_title');?>
						<!--end-title_content-->
					<?endif;?>
					<?$APPLICATION->ShowViewContent('top_section_filter_content');?>
				<?endif; // if !$isIndex && !$is404 && !$isForm?>

				<div class="container <?=($isCabinet ? 'cabinte-page' : '');?><?=($isBlog ? ' blog-page' : '');?> <?=CAllcorp2::ShowPageProps("ERROR_404");?>" style="width:1600px;" rel="tttyyy">
					<?if(!$isIndex):?>
						<div class="row">
							<?if($APPLICATION->GetProperty("FULLWIDTH")!=='Y'):?>
								<div class="maxwidth-theme">
							<?endif;?>
							<?if($is404):?>
								<div class="col-md-12 col-sm-12 col-xs-12 content-md">
							<?else:?>
								<div class="col-md-12 col-sm-12 col-xs-12 content-md">
									<?if(CSite::InDir('/partners/')):?>
										<div class="right_block narrow_Y  catalog_page">
									<?else:?>
										<div class="right_block narrow_<?=CAllcorp2::ShowPageProps("MENU");?> <?=$APPLICATION->ShowViewContent('right_block_class')?> <?=$isCatalog ? "catalog_page" : ""?>">
									<?endif;?>
									<?CAllcorp2::get_banners_position('CONTENT_TOP');?>

											<?ob_start();?>
										<div>
										<?$APPLICATION->IncludeComponent(
											"bitrix:menu", 
				"probniy_shablon_menu", 
											array(
												"ROOT_MENU_TYPE" => "left",
												"MENU_CACHE_TYPE" => "A",
												"MENU_CACHE_TIME" => "3600000",
												"MENU_CACHE_USE_GROUPS" => "N",
												"MENU_CACHE_GET_VARS" => array(
												),
												"MAX_LEVEL" => "1",
												"CHILD_MENU_TYPE" => "podrazdel",
												"USE_EXT" => "Y",
												"DELAY" => "N",
												"ALLOW_MULTI_SELECT" => "N",
												"COMPONENT_TEMPLATE" => "probniy_shablon_menu",
												"COMPOSITE_FRAME_MODE" => "Y",
												"COMPOSITE_FRAME_TYPE" => "STATIC"
											),
											false
			);?></div>
											<?$sMenuContent = ob_get_contents();
											ob_end_clean();?>
							<?endif;?>
					<?endif;?>
					<?CAllcorp2::checkRestartBuffer();?>