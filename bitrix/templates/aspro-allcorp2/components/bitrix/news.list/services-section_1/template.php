<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?if($arResult['SECTIONS']):?>
<?if (CSite::InDir('/services/index.php')):?>
<div class="text_before_items">
	<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/topinc.php"
	)
);?>
</div>
<?endif;?>
<?//echo "<pre>".print_r($arResult['SECTIONS'],true)."</pre>";?>
	<div class="item-views catalog sections" rel="tttyyy">
		<div class="items row row_block services flexbox">
			<?$arParams["LINE_ELEMENT_COUNT"] = intval($arParams["LINE_ELEMENT_COUNT"]);
			$arParams["LINE_ELEMENT_COUNT"] = (($arParams["LINE_ELEMENT_COUNT"] > 0 && $arParams["LINE_ELEMENT_COUNT"] < 12) ? $arParams["LINE_ELEMENT_COUNT"] : 3);
			$colmd = floor(12 / $arParams['LINE_ELEMENT_COUNT']);
			$colsm = floor(12 / round($arParams['LINE_ELEMENT_COUNT'] / 2));?>
			<?foreach($arResult['SECTIONS'] as $arItem):?>
				<?
				// edit/add/delete buttons for edit mode
				$arSectionButtons = CIBlock::GetPanelButtons($arItem['IBLOCK_ID'], 0, $arItem['ID'], array('SESSID' => false, 'CATALOG' => true));
				$this->AddEditAction($arItem['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'SECTION_EDIT'));
				$this->AddDeleteAction($arItem['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				// preview picture
				if($bShowSectionImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE'])){
					$bImage = strlen($arItem['~PICTURE']);
					$arSectionImage = ($bImage ? CFile::ResizeImageGet($arItem['~PICTURE'], array('width' => 700, 'height' => 700), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array());
					$imageSectionSrc = ($bImage ? $arSectionImage['src'] : SITE_TEMPLATE_PATH.'/images/noimage_sections.png');
				}
				?>
				<div class="col-md-2<?//=$colmd?> col-sm-<?=$colsm;?>">
					<div class="item <?=($bShowSectionImage ? '' : ' wti')?>  slice-item <?=$arParams['IMAGE_CATALOG_POSITION'];?>" id="<?=$this->GetEditAreaId($arItem['ID'])?>" style="padding: 0px 0px 0px;">
						<?// icon or preview picture?>
						<?if($bShowSectionImage):?>
						<div class="image" style="width:100%;margin-bottom:10px;border-radius:0;">
								<?if (CSite::InDir('/services')):?>
								<a href="<?=$arItem['SECTION_PAGE_URL']?>">
								<?else:?>
									<a href="<?=$arItem['CODE']?>/">
								<?endif;?>
									<div class="img_block" style="background-image: url(<?=$imageSectionSrc?>);"></div>

									<?/* <img class="image image-edit" src="<?=$imageSectionSrc?>" alt="<?=( $arItem['PICTURE']['ALT'] ? $arItem['PICTURE']['ALT'] : $arItem['NAME']);?>" title="<?=( $arItem['PICTURE']['TITLE'] ? $arItem['PICTURE']['TITLE'] : $arItem['NAME']);?>" class="img-responsive" />*/?>
								</a>
							</div>
						<?endif;?>

						<div class="info title_edit">
							<?// section name?>
							<?if(in_array('NAME', $arParams['FIELD_CODE'])):?>
								<div class="title">
									<?if (CSite::InDir('/services')):?>
										<a href="<?=$arItem['SECTION_PAGE_URL']?>" class="dark-color text-edit">
									<?else:?>
											<a href="<?=$arItem['CODE']?>/" class="dark-color text-edit">
									<?endif;?>
										<?=$arItem['NAME']?>
									</a>
								</div>
							<?endif;?>

							<?// section child?>
							<?/*if($arItem['CHILD']):?>
								<div class="text childs">
									<ul>
										<?foreach($arItem['CHILD'] as $arSubItem):?>
											<li><a class="colored" href="<?=($arSubItem['SECTION_PAGE_URL'] ? $arSubItem['SECTION_PAGE_URL'] : $arSubItem['DETAIL_PAGE_URL'] );?>"><?=$arSubItem['NAME']?></a></li>
										<?endforeach;?>
									</ul>
								</div>
							<?endif;*/?>
							
							<?// section preview text?>
							<?if(strlen($arItem['UF_TOP_SEO']) && $arParams['SHOW_SECTION_PREVIEW_DESCRIPTION'] != 'N'):?>
								<div class="text">
									<?=$arItem['UF_TOP_SEO']?>
								</div>
							<?endif;?>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
			<?if (CSite::InDir('/services/index.php')):?>
<div class="text_before_items">
<?$APPLICATION->IncludeComponent("bitrix:main.include", "template3", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
		"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "/include/bottomInc.php",	// Путь к файлу области
	),
	false
);?>
</div>
<?endif;?>
<?endif;?>