<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>
<?$this->setFrameMode(true);?>
<?// intro text?>
<div class="block_before_items" style="margin-bottom:20px;">
	<img style="width:100%;" src="<?=CFile::GetPath(CIBlock::GetArrayByID("10", "PICTURE"))?>">
</div>
<div class="block_before_items">
	<div><?//echo "<pre>".print_r($arParams,true)."</pre>"?></div>
	<div class="text_before_items">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_DIR."include/reviews.php",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
	<div class="btns">
		<span class="btn btn-default btn-lg animate-load" data-event="jqm" data-param-id="<?=CAllcorp2::getFormID("aspro_allcorp2_feedback");?>" data-name="feedback"><span><?=(strlen($arParams['S_FEEDBACK']) ? $arParams['S_FEEDBACK'] : GetMessage('S_FEEDBACK'))?></span></span>
	</div>
</div>
<?
$arItemFilter = CAllcorp2::GetIBlockAllElementsFilter($arParams);
$itemsCnt = CCache::CIblockElement_GetList(array("CACHE" => array("TAG" => CCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), $arItemFilter, array());
?>
<?if(!$itemsCnt):?>
	<div class="alert alert-warning"><?=GetMessage("SECTION_EMPTY")?></div>
<?else:?>
	<?CAllcorp2::CheckComponentTemplatePageBlocksParams($arParams, __DIR__);?>
	<?// section elements?>
	<?@include_once('page_blocks/'.$arParams["SECTION_ELEMENTS_TYPE_VIEW"].'.php');?>
<?endif;?>