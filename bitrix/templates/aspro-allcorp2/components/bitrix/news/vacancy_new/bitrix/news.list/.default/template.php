<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="item-views accordion accordion-type-block image_right vacancy">
  <div class="group-content">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?$r2=$arItem['IBLOCK_SECTION_ID'];
	if($r1!=$r2):?>
	<div class="tab-pane">
	<?$res = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID']);
		if($ar_res = $res->GetNext())?>
		<h3 style="color:#0088cc;"><?=$ar_res['NAME']?></h3>
	<?endif;?>
		<div class="accordion-type-1">
			<div class="item wti">
					<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
							<div class="accordion-head" style="font-size:18px;">
								<span><?echo $arItem["NAME"]?><i class="fa fa-angle-down"></i></span>
								<div class="pay">
									&nbsp;<b><?echo $arItem["DISPLAY_PROPERTIES"]["PAY"]["DISPLAY_VALUE"];?></b>
								</div>
							</div>
					<?endif;?>
					<div class="panel-collapse in">
						<div class="accordion-body">
							<div class="row">
								<div class="col-md-12" style="margin-bottom:20px;">
								<?echo $arItem["FIELDS"]["DETAIL_TEXT"];?>
	
								<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
									<div class="properties">
										<div class="inner-wrapper">
											<div class="property  pay" style="font-size:1.2em;">
												<?=$arProperty["NAME"]?>:&nbsp;
												<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
													<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
												<?else:?>
													<?=$arProperty["DISPLAY_VALUE"];?>
												<?endif?>
											</div>
										</div>
									</div>
								<?endforeach;?>


								<button class="btn btn-default" data-event="jqm" data-name="resume" data-param-id="8" data-autoload-POST="<?=CAllcorp2::formatJsName($arItem['NAME'])?>" data-autohide="">Отправить резюме</button>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	<?if($r1!=$r2):?>
	</div>
	<?endif;?>
	<?$r1=$arItem['IBLOCK_SECTION_ID'];?>
	<?endforeach;?>
  </div>
</div>
<script> 
	$(document).ready(function() {
		$(".resume_frame >div>div>.popup>.form-header>.text> #title_form").html("Отправить резюме");
		$("div.accordion-body").hide();
		$(".accordion-head").click(function(){
			$(this).next().children('div.accordion-body').toggle('slow');
		});
	});
</script>