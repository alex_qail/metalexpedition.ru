<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<?
global $arTheme;
$expandedLink = $arTheme['HEADER_MOBILE_MENU_EXPAND']['VALUE'];



/*
$arResult[0]["CHILD"][0]["SELECTED"] = "";
*/

//пример выборки дерева подразделов для раздела 

//$arSect['SECTION_PAGE_URL']
$page = $APPLICATION->GetCurPage();
$p = explode("/", $page);
//echo "<pre>".print_r($p,true)."</pre>"; 
$arResult[5] = [];
$arResult[6] = [];
$codes = ['catalogg', 'proizvodstvo', 'aspro_allcorp2_services'];
foreach($codes as $key => $code) { 
	$resc = CIBlock::GetList(Array(), Array('CODE' => $code), false);
	if($arrc = $resc->Fetch())
		$c_id=$arrc["ID"];

	switch ($key) {
		case 0:
			$nc = 0;
			$arResult[$nc]["TEXT"] = "Каталог";
			$arResult[$nc]["LINK"] = "/product/";
			$arResult[$nc]["CHAIN"][0] = "Продукты";
			if($p[1]=="product")
				$arResult[$nc]["SELECTED"] = 1;
			else
				$arResult[$nc]["SELECTED"] = 0;
			break;
		case 1:
			$nc = 5;
			$arResult[$nc]["TEXT"] = "Производство";
			$arResult[$nc]["LINK"] = "/proizvodstvo/";
			$arResult[$nc]["CHAIN"][0] = "Производство";
			if($p[1]=="proizvodstvo")
				$arResult[$nc]["SELECTED"] = 1;
			else
				$arResult[$nc]["SELECTED"] = 0;
			break;
		case 2:
			$nc = 6;
			$arResult[$nc]["TEXT"] = "Услуги";
			$arResult[$nc]["LINK"] = "/services/";
			$arResult[$nc]["CHAIN"][0] = "Услуги";
			if($p[1]=="services")
				$arResult[$nc]["SELECTED"] = 1;
			else
				$arResult[$nc]["SELECTED"] = 0;
			break;
	} 


	$arResult[$nc]["PERMISSION"] = "X";
	$arResult[$nc]["ADDITIONAL_LINKS"] = [];
	$arResult[$nc]["ITEM_TYPE"] = "D";
	$arResult[$nc]["ITEM_INDEX"] = 0;
	$arResult[$nc]["PARAMS"] = [];
	$arResult[$nc]["PARAMS"] = [];
	$arResult[$nc]["DEPTH_LEVEL"] = 1;
	$arResult[$nc]["IS_PARENT"] = 1;

	$rsParentSection = CIBlockSection::GetByID($c_id);
	if ($arParentSection = $rsParentSection->GetNext())
	{
	   $arFilter = array('IBLOCK_ID' => $c_id,'=DEPTH_LEVEL' => 1,'=ACTIVE' => 'Y'); 
	   $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
		$i=0;
	   while ($arSect = $rsSect->GetNext())
	   {
			$arResult[$nc]["CHILD"][$i]["TEXT"] = $arSect['NAME'];
			$arResult[$nc]["CHILD"][$i]["LINK"] = $arSect['SECTION_PAGE_URL'];
			$arResult[$nc]["CHILD"][$i]["PERMISSION"] = "X";
			$arResult[$nc]["CHILD"][$i]["ADDITIONAL_LINKS"] = [];
			$arResult[$nc]["CHILD"][$i]["ITEM_TYPE"] = "P";
			$arResult[$nc]["CHILD"][$i]["ITEM_INDEX"] = $i;
			$arResult[$nc]["CHILD"][$i]["PARAMS"] = [];
			$arResult[$nc]["CHILD"][$i]["CHAIN"][0] = $arResult[$nc]["CHAIN"][0];
			$arResult[$nc]["CHILD"][$i]["CHAIN"][1] = $arResult[$nc]["CHAIN"][0];
			$arResult[$nc]["CHILD"][$i]["DEPTH_LEVEL"] = 2;
			$arResult[$nc]["CHILD"][$i]["IS_PARENT"] = "";
			$arResult[$nc]["CHILD"][$i]["SELECTED"] = "";
			$i++;
	   }
	}
}


?>
<?//echo "<pre>".print_r($arResult[5],true)."</pre>";?>
<?if($arResult):?>
	<div class="menu top">
		<ul class="top">
			<?foreach($arResult as $arItem):?>
				<?$bShowChilds = $arParams['MAX_LEVEL'] > 1;
				if($expandedLink == $arItem['LINK'] && $arParams['SHORT_MENU'] != 'Y')
					continue;
				?>
				<?$bParent = $arItem['CHILD'] && $bShowChilds;?>
				<li<?=($arItem['SELECTED'] ? ' class="selected"' : '')?>>
					<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>">
						<span><?=$arItem['TEXT']?></span>
						<?if($bParent):?>
							<span class="arrow"><i class="svg svg_triangle_right"></i></span>
						<?endif;?>
					</a>
					<?if($bParent):?>
						<ul class="dropdown">
							<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right_white.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
							<li class="menu_title"><a href="<?=$arItem["LINK"]?>"><?=$arItem['TEXT']?></a></li>
							<?foreach($arItem['CHILD'] as $arSubItem):?>
								<?$bShowChilds = $arParams['MAX_LEVEL'] > 2;?>
								<?$bParent = $arSubItem['CHILD'] && $bShowChilds;?>
								<li<?=($arSubItem['SELECTED'] ? ' class="selected"' : '')?>>
									<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubItem["LINK"]?>" title="<?=$arSubItem["TEXT"]?>">
										<span><?=$arSubItem['TEXT']?></span>
										<?if($bParent):?>
											<span class="arrow"><i class="svg svg_triangle_right"></i></span>
										<?endif;?>
									</a>
									<?if($bParent):?>
										<ul class="dropdown">
											<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
											<li class="menu_title"><a href="<?=$arSubItem["LINK"]?>"><?=$arSubItem['TEXT']?></a></li>
											<?foreach($arSubItem["CHILD"] as $arSubSubItem):?>
												<?$bShowChilds = $arParams['MAX_LEVEL'] > 3;?>
												<?$bParent = $arSubSubItem['CHILD'] && $bShowChilds;?>
												<li<?=($arSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
													<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubSubItem["LINK"]?>" title="<?=$arSubSubItem["TEXT"]?>">
														<span><?=$arSubSubItem['TEXT']?></span>
														<?if($bParent):?>
															<span class="arrow"><i class="svg svg_triangle_right"></i></span>
														<?endif;?>
													</a>
													<?if($bParent):?>
														<ul class="dropdown">
															<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
															<li class="menu_title"><a href="<?=$arSubSubItem["LINK"]?>"><?=$arSubSubItem['TEXT']?></a></li>
															<?foreach($arSubSubItem["CHILD"] as $arSubSubSubItem):?>
																<li<?=($arSubSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
																	<a class="dark-color" href="<?=$arSubSubSubItem["LINK"]?>" title="<?=$arSubSubSubItem["TEXT"]?>">
																		<span><?=$arSubSubSubItem['TEXT']?></span>
																	</a>
																</li>
															<?endforeach;?>
														</ul>
													<?endif;?>
												</li>
											<?endforeach;?>
										</ul>
									<?endif;?>
								</li>
							<?endforeach;?>
						</ul>
					<?endif;?>
				</li>
			<?endforeach;?>
		</ul>
	</div>
<?endif;?>