<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$SNIPPETS = Array();
$SNIPPETS['snippet0001.snp'] = Array('title' => 'Horizontal colored line');
$SNIPPETS['snippet0002.snp'] = Array('title' => 'Blockquote');
$SNIPPETS['snippet0003.snp'] = Array('title' => 'Table with border');
$SNIPPETS['snippet0004.snp'] = Array('title' => 'Table');
$SNIPPETS['snippet0005.snp'] = Array('title' => 'Danger block');
$SNIPPETS['snippet0006.snp'] = Array('title' => 'Info block');
$SNIPPETS['snippet0007.snp'] = Array('title' => 'Текущий город');
$SNIPPETS['snippet0008.snp'] = Array('title' => 'Локальный номер');
$SNIPPETS['snippet0009.snp'] = Array('title' => 'Название раздела с маленькой буквы');
$SNIPPETS['snippet0010.snp'] = Array('title' => 'Название раздела');
$SNIPPETS['snippet0011.snp'] = Array('title' => 'Название раздела с большой буквы');
?>